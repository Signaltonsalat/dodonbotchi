import logging as log
import os
import random

from time import sleep

from .mame import Ddonpach, get_action_str
from .util import ensure_directories


def ddonpai_step(ddonpach, last, current):
    vert = random.choice([0, 1, 2])
    hori = random.choice([0, 1, 2])
    shot = random.choice([0, 1])
    action = get_action_str(vert=vert, hori=hori, shot=shot)
    ddonpach.send_action(action)


def ddonpai_loop(ddonpach):
    last_observation = ddonpach.read_observation()
    ddonpach.send_action(get_action_str(shot=1))
    while True:
        observation = ddonpach.read_observation()
        ddonpai_step(ddonpach, last_observation, observation)
        last_observation = observation


def play(ddonpai_dir):
    ensure_directories(ddonpai_dir)
    ddonpai_dir = os.path.abspath(ddonpai_dir)

    ddonpach = Ddonpach()
    ddonpach.inp_dir = ddonpai_dir
    ddonpach.snp_dir = ddonpai_dir

    ddonpach.start_mame()

    ddonpai_loop(ddonpach)
